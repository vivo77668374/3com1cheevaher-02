const int redPin = 4;  
const int greenPin = 5; 
const int bluePin = 6;  

void setup() {
  Serial.begin(9600);
  pinMode(redPin, OUTPUT);
  pinMode(greenPin, OUTPUT);
  pinMode(bluePin, OUTPUT);
  
  digitalWrite(redPin, LOW);
  digitalWrite(greenPin, LOW);
  digitalWrite(bluePin, LOW);
}

void loop() {
  if (Serial.available() > 0) {
    String input = Serial.readStringUntil('\n'); 
    input.trim();

    digitalWrite(redPin, LOW);
    digitalWrite(greenPin, LOW);
    digitalWrite(bluePin, LOW);
    
    if (input == "r") { 
      digitalWrite(redPin, HIGH);
    } else if (input == "g") { 
      digitalWrite(greenPin, HIGH);
    } else if (input == "b") { 
      digitalWrite(bluePin, HIGH);
    } else if (input == "on") { 
      digitalWrite(redPin, HIGH);
      digitalWrite(greenPin, HIGH);
      digitalWrite(bluePin, HIGH);
    } else if (input == "off") {
      
    } else {
     
    }
  }
}